using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace Laba9
{
    class Program
    {
        static public int ChoosingSportSign()
        {
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Clear();
                Console.WriteLine("   Выберите вид спорта:\n 1  - Шахматы\n 2  - Футбол\n 3  - Хоккей\n 4  - Баскетбол\n 5  - Поло");
                int q = int.Parse(Console.ReadLine());
                if (q > 0 && q < 13)
                    return q;
            }
        }

        static void Main(string[] args)
        {
            int i, j;
            StreamReader f = new StreamReader("text.txt");
            String S = f.ReadToEnd();
            f.Close();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("   Исходные Данные:\n\n" + S);
            Console.WriteLine();
            string Sep = @"[\r][\n]";
            string Sep2 = @"[\r][\n]|[;]";
            Regex r = new Regex(Sep);
            Regex r2 = new Regex(Sep2);
            string[] s1 = r.Split(S);
            string[] ss = r2.Split(S);
            Sportsmen[] f1 = new Sportsmen[s1.Length];
            j = 0;
            for (i = 0; i < s1.Length && j < ss.Length - 3; i++)
            {
                f1[i].fio = ss[j];
                f1[i].birthday = ss[j + 1];
                f1[i].telephone = ss[j + 3];
                try
                {
                    f1[i].sing = (Sport)Enum.Parse(typeof(Sport), ss[j + 2]);
                }
                catch (ArgumentException error)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Информация в файле не соответствует перечислению: " + error.Message);
                    f1[i].sing = Sport.Null;
                    Console.ResetColor();
                }
                j = j + 4;
            }
            Console.ReadLine();
            Console.Clear();
            Array.Sort(f1, new SortFam());
            Console.ForegroundColor = ConsoleColor.Green;
            string tabl1 = "┌───────────────┬────────────┬────────────┐";
            string tabl2 = "│     Фио       │    Дата    │   Разряд   │";
            string tabl3 = "├───────────────┼────────────┼────────────┤";
            string tabl4 = "└───────────────┴────────────┴────────────┘";
            Console.WriteLine(tabl1 + "\n" + tabl2 + "\n" + tabl3);
            for (i = 0; i < f1.Length; i++)
                f1[i].Info();
            Console.WriteLine("└───────────────┴────────────┴────────────┘");
            Console.ReadLine();
            Console.Clear();
            StreamWriter q;
            bool qwe;
            Console.WriteLine("Выполняется создание файлов");
            System.Threading.Thread.Sleep(1000);
            for (Sport w = Sport.Шахматы; w < Sport.Null; w++)
            {
                qwe = false;
                for (i = 0; i < f1.Length; i++)
                    if (w == f1[i].sing)
                        qwe = true;
                if (qwe)
                {
                    q = new StreamWriter(Convert.ToString(w) + ".txt");
                    q.WriteLine(tabl1);
                    q.WriteLine(tabl2);
                    q.WriteLine(tabl3);
                    
                    
                    for (i = 0; i < f1.Length; i++)
                        if (w == f1[i].sing)
                            q.WriteLine("│{0,15}│{1,12}│{2,12}│", f1[i].fio, f1[i].birthday, f1[i].telephone);
                    q.WriteLine(tabl4);
                    Console.WriteLine("Файл " + Convert.ToString(w) + ".txt" + " создан");
                    q.Close();
                }
            }
            Console.WriteLine("Нажмите любую клавишу, чтобы продолжить...");
            Console.ReadKey();

            int n = ChoosingSportSign();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("  Выбор формата ввода:\nHome-будет выводиться фамилия и возраст.\nEnd – фамилия, дата рождения и разряд.");
            ConsoleKeyInfo k = Console.ReadKey();
            qwe = true;
            Console.WriteLine();
            if (k.Key == ConsoleKey.Home)
            {
                Console.Clear();
                Console.WriteLine("┌───────────────┬─────────┐");
                Console.WriteLine("│     Фио       │ Возраст │");
                Console.WriteLine("├───────────────┼─────────┤");
                for (i = 0; i < f1.Length; i++)
                {
                    if (Enum.GetName(typeof(Sport), n - 1) == Convert.ToString(f1[i].sing))
                    { Console.WriteLine("│{0,15}│{1,9}│", f1[i].fio, f1[i].Years); qwe = false; }
                }
                Console.WriteLine("└───────────────┴─────────┘");
            }
            if (k.Key == ConsoleKey.End)
            {
                Console.Clear();
                Console.WriteLine("┌───────────────┬────────────────┬────────────┐");
                Console.WriteLine("│     Фио       │  Дата рождения │   Разряд   │");
                Console.WriteLine("├───────────────┼────────────────┼────────────┤");
                for (i = 0; i < f1.Length; i++)
                {
                    if (Enum.GetName(typeof(Sport), n - 1) == Convert.ToString(f1[i].sing))
                    { Console.WriteLine("│{0,15}│{1,16}│{2,12}│", f1[i].fio, f1[i].birthday, f1[i].telephone); qwe = false; }
                }
                Console.WriteLine("└───────────────┴────────────────┴────────────┘");
            }
            if (k.Key != ConsoleKey.Home && k.Key != ConsoleKey.End)
            {
                Console.Clear();
                Console.WriteLine("Неверный выбор формата ввода!");
            }
            else
                if (qwe)
                {
                    Console.Clear();
                    Console.WriteLine("Информация поданному знаку зодиака отсутствует.");
                }
            Console.ReadKey();

        }
    }
}
