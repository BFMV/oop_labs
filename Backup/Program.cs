using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace Laba9
{
    //Перечисление
    public enum zodiak
    { 
        Козерог, Водолей, Рыбы, Овен, Телец, Близнецы, Рак, Лев, Дева, Весы, Скорпион, Стрелец, Null
    }
     public struct Friends
    {
        public string fio;
        public string birthday;
        public zodiak znak;
        public string telephone;
        //свойство для определения возраста
         public int vozrast
         {
             get
             {
                int ret = DateTime.Now.Year - Convert.ToDateTime(birthday).Year;
                 if (DateTime.Now.DayOfYear < Convert.ToDateTime(birthday).DayOfYear)
                     ret--;
                 return ret;
             }
         }
         //Свойство для опред количества дней до дня рождения
         public int KolDayDoDR
         {
             get
             {
                 int ret = DateTime.Now.DayOfYear - Convert.ToDateTime(birthday).DayOfYear;
                 if (ret < 0)
                     ret += 365;
                 return ret;
             }
         }
         //Методы для вывода информации
         public void info()
         {
             if(znak != zodiak.Null)
                 Console.WriteLine("│{0,15}│{1,12}│{2,12}│", fio, birthday, telephone);
         }
         public void info(zodiak z)
         {
             if (z == znak)
                 Console.WriteLine("{0}", fio);
         }
    }
    //Класс для сортировки по фамилии
    class SortFam : System.Collections.IComparer 
    {
        public int Compare(object ob1, object ob2)
        {
            Friends s1 = (Friends)ob1;
            Friends s2 = (Friends)ob2;
            return String.Compare(s1.fio, s2.fio);
        }
    }
    class Program
    {
        static public int ViborZodiak()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("   Выберите знак зодиака:\n 1  - Козерог\n 2  - Водолей\n 3  - Рыбы\n 4  - Овен\n 5  - Телец\n 6  - Близнецы\n 7  - Рак\n 8  - Лев\n 9  - Дева\n 10 - Весы\n 11 - Скорпион\n 12 - Стрелец");
                int q = int.Parse(Console.ReadLine());
                if(q>0 && q<13)
                    return q;
            }
        }
        static void Main(string[] args)
        {
            int i, j;
            StreamReader f = new StreamReader("text.txt");
            String S = f.ReadToEnd();
            f.Close();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("   Исходная строка:\n" + S);
            Console.WriteLine();
            Console.ResetColor();
            string Sep = @"[\r][\n]";
            string Sep2 = @"[\r][\n]|[;]";
            Regex r = new Regex(Sep);
            Regex r2= new Regex(Sep2);
            string[] s1 = r.Split(S);
            string[] ss = r2.Split(S);
            Friends[] f1 = new Friends[s1.Length];
            j = 0;
            for (i = 0; i < s1.Length && j<ss.Length-3; i++)
            { 
                f1[i].fio = ss[j];
                f1[i].birthday  = ss[j+1];
                f1[i].telephone = ss[j + 3];                
                //Конвертируем строку в паречисление
                try
                {
                    f1[i].znak = (zodiak)Enum.Parse(typeof(zodiak), ss[j + 2]);
                }
                catch (ArgumentException error)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Информация в файле не соответствует перечислению: " + error.Message);
                    f1[i].znak = zodiak.Null;
                    Console.ResetColor();
                }
                j = j + 4;
            }
            //Array.Sort(f1);
            Array.Sort(f1, new SortFam()); 
            string tabl1 = "┌───────────────┬────────────┬────────────┐";
            string tabl2 = "│     Фио       │    Дата    │  Телефон   │";
            string tabl3 = "├───────────────┼────────────┼────────────┤";
            string tabl4 = "└───────────────┴────────────┴────────────┘";
            Console.WriteLine(tabl1+ "\n" +tabl2+"\n"+tabl3);
            for (i = 0; i < f1.Length; i++)
                f1[i].info();
            Console.WriteLine("└───────────────┴────────────┴────────────┘");
            StreamWriter q;
            bool qwe;
            for (zodiak w = zodiak.Козерог; w < zodiak.Null; w++)
            {
                qwe = false;
                for (i = 0; i < f1.Length; i++)
                    if (w == f1[i].znak)
                        qwe = true;
                if (qwe)
                {
                    q = new StreamWriter(Convert.ToString(w) + ".txt");
                    q.WriteLine(tabl1);
                    q.WriteLine(tabl2);
                    q.WriteLine(tabl3);
                    for (i = 0; i < f1.Length; i++)
                        if (w == f1[i].znak)
                            q.WriteLine("│{0,15}│{1,12}│{2,12}│", f1[i].fio, f1[i].birthday, f1[i].telephone);
                    q.WriteLine(tabl4);
                    Console.WriteLine("Файл " + Convert.ToString(w) + ".txt" + " создан");
                    q.Close();
                }
            }
            Console.WriteLine("Нажмите любую клавишу, чтобы продолжить...");
            Console.ReadKey();
            int n = ViborZodiak();
            Console.WriteLine("  Выбор формата ввода:\n нажмите Home-будет выводиться фамилия и возраст.\n нажмите End – фамилия, количество дней до ближайшего дня рождения и номер телефона.");
            ConsoleKeyInfo k = Console.ReadKey();
            qwe = true;
            Console.WriteLine();
            if (k.Key == ConsoleKey.Home)
            {
                Console.WriteLine("┌───────────────┬─────────┐");
                Console.WriteLine("│     Фио       │ Возраст │");
                Console.WriteLine("├───────────────┼─────────┤");
                for (i = 0; i < f1.Length; i++)
                {
                    if (Enum.GetName(typeof(zodiak), n-1) == Convert.ToString(f1[i].znak))
                    {Console.WriteLine("│{0,15}│{1,9}│", f1[i].fio, f1[i].vozrast); qwe = false;}
                }
                Console.WriteLine("└───────────────┴─────────┘");
            }
            if (k.Key == ConsoleKey.End)
            {
                Console.WriteLine("┌───────────────┬────────────────┬────────────┐");
                Console.WriteLine("│     Фио       │День рожд через:│  Телефон   │");
                Console.WriteLine("├───────────────┼────────────────┼────────────┤");
                for (i = 0; i < f1.Length; i++)
                {
                    if (Enum.GetName(typeof(zodiak), n-1) == Convert.ToString(f1[i].znak))
                    { Console.WriteLine("│{0,15}│{1,16}│{2,12}│", f1[i].fio, f1[i].KolDayDoDR, f1[i].telephone); qwe = false; }
                }
                Console.WriteLine("└───────────────┴────────────────┴────────────┘");
            }
            if (k.Key != ConsoleKey.Home && k.Key != ConsoleKey.End)
            {
                Console.Clear();
                Console.WriteLine("Неверный выбор формата ввода!");
            }
            else
                if (qwe)
                {
                    Console.Clear();
                    Console.WriteLine("Информация поданному знаку зодиака отсутствует.");
                }
            Console.ReadKey();
           
        }
    }
}
