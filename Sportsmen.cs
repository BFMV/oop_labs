﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laba9
{
    /// <summary>
    /// Перечисления видов спорта
    /// </summary>
    public enum Sport
    {
        Шахматы, Футбол, Хоккей, Баскетбол, Поло, Null
    }

    public struct Sportsmen
    {
        public string fio;
        public string birthday;
        public Sport sing;
        public string telephone;
        /// <summary>
        /// Свойство для определения возроста
        /// </summary>
        public int Years
        {
            get
            {
                int ret = DateTime.Now.Year - Convert.ToDateTime(birthday).Year;
                if (DateTime.Now.DayOfYear < Convert.ToDateTime(birthday).DayOfYear)
                    ret--;
                return ret;
            }
        }

        /// <summary>
        /// Метод для вывода информации
        /// </summary>
        public void Info()
        {
            if (sing != Sport.Null)
                Console.WriteLine("│{0,15}│{1,12}│{2,12}│", fio, birthday, telephone);
        }
        
        /// <summary>
        /// Информация о спортсмене
        /// </summary>
        /// <param name="z">Спортсмен</param>
        public void Info(Sport z)
        {
            if (z == sing)
                Console.WriteLine("{0}", fio);
        }
    }

    /// <summary>
    /// Класс сортировки
    /// </summary>
    class SortFam : System.Collections.IComparer
    {
        /// <summary>
        /// Сортировка по фамилии
        /// </summary>
        /// <param name="ob1">Фамилия 1</param>
        /// <param name="ob2">Фамилия 2</param>
        /// <returns></returns>
        public int Compare(object ob1, object ob2)
        {
            Sportsmen s1 = (Sportsmen)ob1;
            Sportsmen s2 = (Sportsmen)ob2;
            return String.Compare(s1.fio, s2.fio);
        }
    }

}
