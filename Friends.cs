﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Laba9
{
    public enum Sport
    {
        Шахматы, Футбол, Хоккей, Баскетбол, Поло, Null
    }
    public struct Friends
    {
        public string fio;
        public string birthday;
        public Sport sing;
        public string telephone;
        //свойство для определения возраста
        public int Years
        {
            get
            {
                int ret = DateTime.Now.Year - Convert.ToDateTime(birthday).Year;
                if (DateTime.Now.DayOfYear < Convert.ToDateTime(birthday).DayOfYear)
                    ret--;
                return ret;
            }
        }
        //Свойство для опред количества дней до дня рождения
        public int TheNumberOfDaysBeforeTheBirthday
        {
            get
            {
                int ret = DateTime.Now.DayOfYear - Convert.ToDateTime(birthday).DayOfYear;
                if (ret < 0)
                    ret += 365;
                return ret;
            }
        }
        //Методы для вывода информации
        public void Info()
        {
            if (sing != Sport.Null)
                Console.WriteLine("│{0,15}│{1,12}│{2,12}│", fio, birthday, telephone);
        }
        public void Info(Sport z)
        {
            if (z == sing)
                Console.WriteLine("{0}", fio);
        }
    }
    class SortFam : System.Collections.IComparer
    {
        public int Compare(object ob1, object ob2)
        {
            Friends s1 = (Friends)ob1;
            Friends s2 = (Friends)ob2;
            return String.Compare(s1.fio, s2.fio);
        }
    }

}
